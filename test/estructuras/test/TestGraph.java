package estructuras.test;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import model.estructuras.Graph;

public class TestGraph {
	Graph<Integer, Integer, Integer> gp;
	@Before
	public void setUp1() {
		gp = new Graph<Integer, Integer,Integer>();
	}
	public void setUp2() 
	{
		for (int i=1;i<=10;i++)
		{
			gp.addVertex((Integer) i, (Integer) i*5);
		}
	}
	@Test
	public void v() {
		assertEquals(gp.V(), 0);
		for (int i=1;i<=10;i++)
		{
			gp.addVertex((Integer) i*20, (Integer) i*5);
			assertEquals(gp.V(), i);
		}
		
	}
	@Test
	public void e() {
		setUp2();
		assertEquals(gp.E(), 0);
		int contador=1;
		for (int i=1;i<=10;i++)
		{
			for (int j=i+1;j<=10;j++,contador++)
			{
				gp.addEdge((Integer) i, (Integer) j, (Integer) i+j);
				assertEquals(gp.E(), contador);
			}
			
		}
		
	}
	@Test
	public void addVertexAndGetInfoVertex() {
		assertEquals(gp.V(), 0);
		for (int i=1;i<=10;i++)
		{
			gp.addVertex((Integer) i*20, (Integer) i*5);
			if (!gp.getInfoVertex((Integer) i*20).equals((Integer) i*5))
				fail ("No esta bien la información del vertice.");
		}
		
		
	}
	@Test
	public void addEdgeAndGetInfoArc() {
		setUp2();
		assertEquals(gp.E(), 0);
		for (int i=1;i<=10;i++)
		{
			for (int j=i+1;j<=10;j++)
			{
				gp.addEdge((Integer) i, (Integer) j, (Integer) i+j);
				if (!gp.getInfoArc((Integer) i, (Integer) j).equals((Integer) i+j))
					fail("No se agrego correctamente la información del arco");
				if (!gp.getInfoArc((Integer) j, (Integer) i).equals((Integer) i+j))
					fail("No se agrego correctamente la información del arco en dirección contraria");
			}
			
		}
		
	}
	@Test
	public void setVertexAndGetInfoVertex() {
		assertEquals(gp.V(), 0);
		for (int i=1;i<=10;i++)
		{
			gp.addVertex((Integer) i*20, (Integer) i*5);
			if (!gp.getInfoVertex((Integer) i*20).equals((Integer) i*5))
				fail ("No esta bien la información del vertice.");
			gp.setInfoVertex((Integer) i*20, (Integer) i);
			if (!gp.getInfoVertex((Integer) i*20).equals((Integer) i))
				fail ("No sirve setVertex.");
		}
		
		
	}
	@Test
	public void setEdgeAndGetInfoArc() {
		setUp2();
		assertEquals(gp.E(), 0);
		for (int i=1;i<=10;i++)
		{
			for (int j=i+1;j<=10;j++)
			{
				gp.addEdge((Integer) i, (Integer) j, (Integer) i+j);
				if (!gp.getInfoArc((Integer) i, (Integer) j).equals((Integer) i+j))
					fail("No se agrego correctamente la información del arco");
				if (!gp.getInfoArc((Integer) j, (Integer) i).equals((Integer) i+j))
					fail("No se agrego correctamente la información del arco en dirección contraria");
				gp.setInfoArc((Integer) i, (Integer) j, (Integer) i*j);
				if (!gp.getInfoArc((Integer) i, (Integer) j).equals((Integer) i*j))
					fail("No sirve setInfoArc");
				if (!gp.getInfoArc((Integer) j, (Integer) i).equals((Integer) i*j))
					fail("No sirve setInfoArc en dirección contraria");
			}
			
		}
		
	}
	@Test
	public void adj() {
		setUp2();
		assertEquals(gp.E(), 0);
		for (int i=1;i<=10;i++)
		{
			for (int j=i+1;j<=10;j++)
			{
				gp.addEdge((Integer) i, (Integer) j, (Integer) i+j);
				Iterator <Integer> es= gp.adj(i);
				for (int k=1;k<=j;k++)
				{
					if(i==k)
						continue;
					try
					{
						Integer u= es.next();
						if (u!=(Integer) k)
						{
							fail("Adyacente incorrecto "+k);
						}
					}catch (Exception e) {
						fail("Hay menos adyacentes");
					}
				}
			}
			
		}
		
	}

}
