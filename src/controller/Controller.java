package controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

import model.logic.MovingViolationsManager;
import model.vo.Maps;
import view.MovingViolationsManagerView;

public class Controller 
{
	
	/**
	 * View
	 */
	private MovingViolationsManagerView view;
	
	private MovingViolationsManager man;
	
	/**
	 * Sabe si ya se leyeron los datos
	 */
	private boolean leidos; 
	
	private Maps mapa; 
	/**
	 * Constructor inicializa el total de datos en cero, crea la view y la lista
	 */
	public Controller() 
	{
		view = new MovingViolationsManagerView();
		man = new MovingViolationsManager();
	}
	
	
	public void readXML() {
		
	}
	
	
	/**
	 * Corre el menu y seg�n lo que responda el usuario ejecuta
	 */
	public void run() 
	{
		long startTime;
		long endTime;
		long duration;
		
		Scanner sc = new Scanner(System.in);
		boolean fin=false;


		while(!fin) 
		{
			view.printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 0:
				startTime = System.currentTimeMillis();
				try {
					man.readSmallXML();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				System.out.println("Total Vertices: " + man.darTotalVertices());
				System.out.println("Total Arcos: " + man.darTotalArcos());
				System.out.println("Tiempo cargar: " + duration);
				break;
			
			case 1: 
				startTime = System.currentTimeMillis();
				try {
					man.readLargeXML();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				System.out.println("Total Vertices: " + man.darTotalVertices());
				System.out.println("Total Arcos: " + man.darTotalArcos());
				System.out.println("Tiempo cargar: " + duration);
				break;
			case 2: 
				startTime = System.currentTimeMillis();
				try {
					man.cargarJson();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				System.out.println("Total Vertices: " + man.darTotalVertices());
				System.out.println("Total Arcos: " + man.darTotalArcos());
				System.out.println("Tiempo cargar: " + duration);
				break;	
			case 3:
				mapa = new Maps(); 
			case 4:	
				fin = true;
				sc.close();
				break;
			}
		}
	}
	


	/**
	 * Convertir fecha a un objeto LocalDate
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @return objeto LD con fecha
	 */
	private static LocalDate convertirFecha(String fecha)
	{
		return LocalDate.parse(fecha, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
	}


	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaaTHH:mm:ss con dd para dia, mm para mes y aaaa para agno, HH para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora)
	{
		//		"dd/MM/yyyy'T'HH:mm:ss"
		return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("dd/MM/yyyy'T'HH:mm:ss"));
	}
}
