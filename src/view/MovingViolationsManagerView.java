package view;

public class MovingViolationsManagerView {

	/**
	 * Constante con el numero maximo de datos maximo que se deben imprimir en consola
	 */
	public static final int N = 20;
	
	public void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 8----------------------");
		System.out.println("0. Cargar XML Central Washington y pasar al JSON");
		System.out.println("1. Cargar XML MAP y pasar al JSON");
		System.out.println("2. Cargar JSON");
		System.out.println("3. Pintar Mapa");
		System.out.println("4. Salir");
		System.out.println("Digite el numero de opcion para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
	
	public void printMessage(String mensaje) {
		System.out.println(mensaje);
	}

}