package model.vo;

import model.estructuras.Queue;

public class Way {

	private long id;
	
	private Queue<Long> nd; 
	
	public Way(long id) {
		this.id = id;
		nd = new Queue<>();
	}
	
	public Way() {
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Queue<Long> getNd() {
		return nd;
	}

	public void setNd(Queue<Long> nd) {
		this.nd = nd;
	}
	
	
	
}
