package model.vo;

import com.teamdev.jxmaps.*;
import com.teamdev.jxmaps.swing.MapView;

import model.logic.MovingViolationsManager;

import javax.swing.*;
import java.awt.*;
import java.util.Iterator;

public class Maps extends MapView
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Map map; 

	JFrame frame = new JFrame("Washigton DC");

	public Maps() {


		setOnMapReadyHandler(new MapReadyHandler() {

			@Override
			public void onMapReady(MapStatus status) {
				// TODO Auto-generated method stub
				if(status == MapStatus.MAP_STATUS_OK) {

					map = getMap();
					MapOptions mapOptions = new MapOptions(); 
					MapTypeControlOptions controlOptions = new MapTypeControlOptions(); 
					controlOptions.setPosition(ControlPosition.RIGHT_TOP);
					mapOptions.setMapTypeControlOptions(controlOptions);
					map.setOptions(mapOptions);
					map.setCenter(new LatLng(38.8963324, -77.0380200));
					map.setZoom(15);

					Iterable<Long> iterableKeys = MovingViolationsManager.getDataInter().keys();
					Iterator<Long> itKeys = iterableKeys.iterator();

					while(itKeys.hasNext()) {
						long key = itKeys.next();
						Iterator<Long> itVertex = MovingViolationsManager.getGraph().adj(key); 
						LatLng[] path = new LatLng[MovingViolationsManager.getGraph().getMyAdjList().get(key).size()];
						int totalLatLng = 0; 
						boolean agregado = false;


						while(itVertex.hasNext()) {
							

							for(int i = 0; i < path.length; i++) {
								Long first =  itVertex.next();
								Interseccion vertice = MovingViolationsManager.getDataInter().get(first);
								path[i] = new LatLng(vertice.getLat(), vertice.getLon()); 
							}
						}

						Polyline polyline = new Polyline(map); 
						polyline.setPath(path);

						PolylineOptions options = new PolylineOptions(); 
						options.setGeodesic(true);

						options.setStrokeColor("#00BFFF");
						options.setStrokeOpacity(1.0);
						options.setStrokeWeight(2.0);

						polyline.setOptions(options);
					}

				}

			}
		});

		frame.add(this, BorderLayout.CENTER);
		frame.setSize(700, 500);
		frame.setVisible(true);
	}
}
