package model.vo;


public class Interseccion 
{
	private long id; 
	
	private double lat, lon; 
	
	public Interseccion(long id, double lat, double lon) {
		this.id = id; 
		this.lat = lat; 
		this.lon = lon; 
	}
	
	public Interseccion() {
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}
	
	
}
