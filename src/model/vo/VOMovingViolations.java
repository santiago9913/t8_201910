package model.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.Date;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations implements Comparable<VOMovingViolations>
{
	/**
	 * Entero del id del objeto y total paid
	 */
	private int objectId, totalPaid,adressId,streetSegId;
	/**
	 * Short fineamt y penalidad
	 */
	private short fineamt, penalty1;
	/**
	 * String de la locaci髇, el identificador de accidente, la descripci髇 de la violaci髇, el identificador de la calle y el identificador de la direcci髇.
	 */
	private String location,  accidentIndicator, violationDescription ;
	/**
	 * String del c骴igo de violacion
	 */
	private String violationCode;
	/**
	 * Fecha del ticket
	 */
	private String ticketIssueDate;
	/**
	 * Identificador del comparador
	 */
	private int cmp;
	
	/**
	 * Coordenada X 
	 */
	private double xCoord; 
	/**
	 * Coordenada Y
	 */
	private double yCoord; 
	
	
	/**
	 * Metodo constructor
	 */
	public VOMovingViolations(int objectId, int totalPaid, short fineamt, String location, String ticketIssueDate, String accidentIndicator, String violationCode,String violationDescription, 
			int streetSegId, int adressId, short penalty1, double xCoord, double yCoord)
	{
		this.objectId = objectId;
		this.totalPaid = totalPaid;
		this.location = location;
		this.accidentIndicator = accidentIndicator;
		this.violationDescription = violationDescription;
		this.penalty1 = penalty1;
		this.ticketIssueDate = ticketIssueDate;
		this.streetSegId = streetSegId;
		this.adressId = adressId;
		this.violationCode = violationCode;
		this.fineamt = fineamt;
		this.xCoord = xCoord; 
		this.yCoord = yCoord;
		cmp=2;
	}

	/**
	 * @return id - Identificador 煤nico de la infracci贸n
	 */
	public int objectId() {
		// TODO Auto-generated method stub
		return objectId;
	}


	/**
	 * @return ViolationCode - Direcci贸n en formato de texto.
	 */
	public String getViolationcode() {
		// TODO Auto-generated method stub
		return violationCode;
	}
	/**
	 * @return  - Direcci贸n en formato de texto.
	 */
	public String getLocation() {
		// TODO Auto-generated method stub
		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infracci贸n .
	 */
	public String getTicketIssueDate() {
		// TODO Auto-generated method stub
		return ticketIssueDate;
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente pag贸 el que recibi贸 la infracci贸n en USD.
	 */
	public int getTotalPaid() {
		// TODO Auto-generated method stub
		return totalPaid;
	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		// TODO Auto-generated method stub
		return accidentIndicator;
	}
	/**
	 * Devuelve TicketIssugeDate como un LocalDate
	 * @return  LocalDateTime
	 */
	public LocalDateTime getTicketIssugeDateLocalTime()
	{
		
		
		return LocalDateTime.parse(ticketIssueDate.substring(0, ticketIssueDate.length()-5), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"));
	}
	/**
	 * Devuelve TicketIssugeDate como un LocalDate
	 * @return  LocalDateTime
	 */
	public LocalDate getTicketIssugeDateLocal()
	{
		
		
		return LocalDate.parse(ticketIssueDate.substring(0, ticketIssueDate.length()-5), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"));
	}
	/**
	 * @return description - Descripci贸n textual de la infracci贸n.
	 */
	public String  getViolationDescription() {
		// TODO Auto-generated method stub
		return violationDescription;
	}
	/**
	 * Devuelve el identificador de la calle
	 * @return identificador de la calle
	 */
	public int getStreetSegId() {
		return streetSegId;
	}
	/**
	 * Devuelve el identificador de la direcci髇
	 * @return identificador de la direcci髇
	 */
	public int getAddressId() {
		return adressId;
	}
	/**
	 * Cambiar el identificador del comparador
	 * @param cmp entero por el cual se va a cambiar
	 */
	public void changeComparator (int cmp )
	{
		this.cmp = cmp;
	}
	/**
	 * Devuelve la penalidad
	 * @return penalidad1
	 */
	public short getPenalty1()
	{
		return penalty1;
	}
	/**
	 * 
	 */
	public double getXCoord()
	{
		return xCoord;
	}
	/**
	 * 
	 */
	public double getYCoord()
	{
		return yCoord;
	}
	
	
	@Override
	public int compareTo(VOMovingViolations o) 
	{

		return comparadores(cmp).compare(this, o);


	}
	@Override
	public String toString()
	{
		// TODO Convertir objeto en String (representacion que se muestra en la consola)
		return "ObjectID:" + objectId + " || Adress ID:" + adressId + " || Street Seg ID:" + streetSegId + " ||  Total Paid:" + totalPaid + " || Location:" + location
				+ " || Ticket Issue Date:" + ticketIssueDate + " || Accident Indicator:" + accidentIndicator 
				+ " || Violation Description:" + violationDescription;
	}
	private Comparator <VOMovingViolations> comparadores (int i)
	{
		
		Comparator<VOMovingViolations> comparador = null; 
		
		//Comparacion por Violation Code
		if (i==1)
		{
			comparador = new Comparator<VOMovingViolations>(){ 
				
				@Override
				public int compare(VOMovingViolations c1, VOMovingViolations c2) 
				{  
					int comparacion =  c1.getViolationcode().compareTo(c2.getViolationcode());
					return  comparacion == 0 ? c1.getTicketIssueDate().compareTo(c2.getTicketIssueDate()) : comparacion ; 
				}} ; 
		}
		//Default
		else if(i==2){
			comparador = new Comparator<VOMovingViolations>(){ 

				@Override
				public int compare(VOMovingViolations c1, VOMovingViolations c2) 
				{ 

					int comparacion = Integer.compare(c1.objectId, c2.objectId); 
					return comparacion;
					
				}} ; 
		}
		//Comparacion por Fechas
		else if(i==3 || i== -3){
			comparador = new Comparator<VOMovingViolations>(){ 

				@Override
				public int compare(VOMovingViolations c1, VOMovingViolations c2) 
				{ 
					String pattern = "yyyy-MM-dd'T'hh:mm:ss";	
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
					LocalDateTime  primero = LocalDateTime.parse(c1.getTicketIssueDate());
					
				
					LocalDateTime  segundo = LocalDateTime.parse(c2.getTicketIssueDate());
					int comparacion = primero.compareTo(segundo)*(i/3);
					return comparacion;
					
				}} ; 
		}
		//Comparaci髇 por Violation Description
		else if (i == 4)
		{
			comparador = new Comparator<VOMovingViolations>(){ 

				@Override
				public int compare(VOMovingViolations c1, VOMovingViolations c2) 
				{ 

					int comparacion = c1.getViolationDescription().compareTo(c2.getViolationDescription()); 
					return comparacion;
					
				}} ; 
		}
		//Comparacion por Violation Code
		else if(i == 5)
		{
			comparador = new Comparator<VOMovingViolations>() {
				
				@Override
				public int compare(VOMovingViolations c1, VOMovingViolations c2) {
					// TODO Auto-generated method stub
					int v1 = Integer.parseInt(c1.violationCode.substring(1));
					int v2 = Integer.parseInt(c2.violationCode.substring(1));
					int comparacion = Integer.compare(v1, v2);
					return comparacion;
				}	
			};
		}
		//Comparacion por Adress ID
		// i= 6 Ascendentemente 
		//i = -6 Descendentemente
		else if(i == 6 || i == -6)
		{
			comparador = new Comparator<VOMovingViolations>() {

				@Override
				public int compare(VOMovingViolations c1, VOMovingViolations c2) {
					// TODO Auto-generated method stub
					int adr1 = c1.adressId;
					int adr2 =c2.adressId;
					int comparacion = Integer.compare(adr1, adr2)*(i/6);
					return comparacion;
				}
			};
		}
		// Comparaci髇 por location
		else if(i == 7 )
		{
			comparador = new Comparator<VOMovingViolations>() {

				@Override
				public int compare(VOMovingViolations c1, VOMovingViolations c2) {
					// TODO Auto-generated method stub
					
					int comparacion = c1.getLocation().compareTo( c2.getLocation());
					if ( comparacion == 0)
					{
						
						comparacion = Integer.compare(c1.getAddressId(),c2.getAddressId());
					}
					return comparacion;
				}
			};
		}
		// Comparacion por xCoord
		else if(i == 8 || i == -8)
		{
			comparador = new Comparator<VOMovingViolations>() {

				@Override
				public int compare(VOMovingViolations o1, VOMovingViolations o2) {
					// TODO Auto-generated method stub
					
					return Double.compare(o1.xCoord, o2.xCoord)*(i/8); 
				
				}
				
				
			};
		}
		else if(i == 9 || i == -9)
		{
			comparador = new Comparator<VOMovingViolations>() {

				@Override
				public int compare(VOMovingViolations o1, VOMovingViolations o2) {
					// TODO Auto-generated method stub
					
					return Double.compare(o1.yCoord, o2.yCoord)*(i/9); 
				
				}
				
				
			};
		}

		return comparador;

	}
	/**
	 * Devuelve el fineamt
	 * @return fineamt
	 */
	public int getFineamt() {
		return fineamt;
	}

	
}
