package model.logic;

import java.awt.BorderLayout;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Iterator;

import javax.swing.*;
import java.awt.*;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import model.estructuras.Graph;
import model.estructuras.Queue;
import model.estructuras.RedBlackBST;
import model.estructuras.Stack;
import model.vo.Interseccion;
import model.vo.Way;

import com.google.gson.*;
import com.teamdev.jxmaps.*;
import com.teamdev.jxmaps.swing.MapView;

public class MovingViolationsManager extends MapView {

	//TODO Definir atributos necesarios

	private static final String XML_CENTRAL_WASHINGTON = "./data/Central-WashingtonDC-OpenStreetMap.xml"; 
	private static final String XML_MAP = "./data/map.xml";
	private static final String JSON ="./data/datos.json";
	private static Graph<Long, double[] ,Double> graphData;

	private static RedBlackBST<Long, Interseccion> dataInter = new RedBlackBST<>();
	private static Queue<Way> dataWay = new Queue<>();

	private double minLatit, minLong, maxLatit, maxLong;
	private String temp; 
	private boolean contieneTag; 
	private boolean wayTag; 
	
	private int c = 0; 
	/**
	 * Metodo constructor
	 */
	public MovingViolationsManager()
	{
		//TODO inicializar los atributos
		graphData = new Graph<>(); 
		minLatit = 0.0; 
		minLong = 0.0; 
		maxLatit = 0.0; 
		maxLong = 0.0;
		contieneTag = false;
		wayTag = false;
		

	}
	
	public void readSmallXML() throws Exception {

		File file = new File(XML_CENTRAL_WASHINGTON);
		SAXParserFactory factory = SAXParserFactory.newInstance(); 
		SAXParser saxParser = factory.newSAXParser(); 
		DefaultHandler handler = new DefaultHandler(){

			Interseccion interseccion; 
			Way way; 

			public void characters(char[] buffer, int start, int lenght) {

				temp = new String(buffer, start, lenght);
			}

			public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

				temp = ""; 

				//Lee la etiqueta bounds con los vertices del mapa
				if(qName.equals("bounds")) {
					minLatit = Double.parseDouble(attributes.getValue("minlat"));
					minLong = Double.parseDouble(attributes.getValue("minlon")); 
					maxLatit = Double.parseDouble(attributes.getValue("maxlat")); 
					maxLong = Double.parseDouble(attributes.getValue("maxlon"));
				}

				//Registra todoas las etiquetas <node> del XML
				else if(qName.equals("node")) {
					double lat = Double.parseDouble(attributes.getValue("lat"));
					double longi = Double.parseDouble(attributes.getValue("lon")); 
					long id = Long.parseLong(attributes.getValue("id"));
					interseccion = new Interseccion(id, lat, longi);
				}	

				//Lee los atributos del tag <way>
				else if(qName.equals("way")) {
					wayTag = true; 
					long id = Long.parseLong(attributes.getValue("id")); 
					way = new Way(id);
				}

				// Lee los atributos del tag <nd> en way 
				else if(qName.equals("nd")) {
					long nd = Long.parseLong(attributes.getValue("ref"));
					way.getNd().enqueue(nd);
				}

				//Lee los atributos del tag <tag> en way 
				else if(qName.equals("tag") && wayTag == true) {
					String key = attributes.getValue("k");
					if(key.equals("highway")) {
						contieneTag = true;  
					}
				}
			}

			public void endElement(String uri, String localName, String qName) throws SAXException {

				if(qName.equals("node")) {
					double [] info = {interseccion.getLat(),interseccion.getLon()};				
					graphData.addVertex(interseccion.getId(), info);
					dataInter.put(interseccion.getId(),interseccion);
				}	
				if(qName.equals("way")) {
					if(contieneTag == true) { // Revisa que el <way> actual contenga el valor <Tag k= "highway">
						dataWay.enqueue(way);
						contieneTag = false; 
						wayTag = false; 
						Iterator<Long> it = way.getNd().iterator(); //Iterador para generar los arcos
						boolean guardadoAnterior = false; 
						long anterior = 0; 
						
						
						while(it.hasNext()){
							long firstKey = it.next(); 
							double lat1 = dataInter.get(firstKey).getLat(); 
							double lon1 = dataInter.get(firstKey).getLon();
							
							
							if(!guardadoAnterior) {
								long secondKey = it.next(); 
								anterior = secondKey; 	
								double lat2 = dataInter.get(secondKey).getLat(); 
								double lon2 = dataInter.get(secondKey).getLon(); 
								double info = Haversine.distance(lat1, lon1, lat2, lon2); 
								graphData.addEdge(firstKey, secondKey, info);	
								guardadoAnterior = true;
							}
							else if(guardadoAnterior) {
								double lat2Ant = dataInter.get(anterior).getLat(); 
								double lon2Ant = dataInter.get(anterior).getLon(); 
								double info = Haversine.distance(lat1, lon1, lat2Ant, lon2Ant); 

								graphData.addEdge(anterior, firstKey, info);
								anterior = firstKey;
								
							}
						}
					}
					else {
						wayTag = false;
					}
				}
			}
		};
		saxParser.parse(file, handler);
		hacerElJSON();
	}

	public void readLargeXML() throws Exception{
		File file = new File(XML_MAP);
		SAXParserFactory factory = SAXParserFactory.newInstance(); 
		SAXParser saxParser = factory.newSAXParser(); 
		DefaultHandler handler = new DefaultHandler(){


			Interseccion interseccion; 
			Way way; 

			public void characters(char[] buffer, int start, int lenght) {

				temp = new String(buffer, start, lenght);
			}

			public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

				temp = ""; 

				//Lee la etiqueta bounds con los vertices del mapa
				if(qName.equals("bounds")) {
					minLatit = Double.parseDouble(attributes.getValue("minlat"));
					minLong = Double.parseDouble(attributes.getValue("minlon")); 
					maxLatit = Double.parseDouble(attributes.getValue("maxlat")); 
					maxLong = Double.parseDouble(attributes.getValue("maxlon"));
				}

				//Registra todoas las etiquetas <node> del XML
				else if(qName.equals("node")) {
					double lat = Double.parseDouble(attributes.getValue("lat"));
					double longi = Double.parseDouble(attributes.getValue("lon")); 
					long id = Long.parseLong(attributes.getValue("id"));
					interseccion = new Interseccion(id, lat, longi);
				}	

				//Lee los atributos del tag <way>
				else if(qName.equals("way")) {
					wayTag = true; 
					long id = Long.parseLong(attributes.getValue("id")); 
					way = new Way(id);
				}

				// Lee los atributos del tag <nd> en way 
				else if(qName.equals("nd")) {
					long nd = Long.parseLong(attributes.getValue("ref"));
					way.getNd().enqueue(nd);
				}

				//Lee los atributos del tag <tag> en way 
				else if(qName.equals("tag") && wayTag == true) {
					String key = attributes.getValue("k");
					if(key.equals("highway")) {
						contieneTag = true;  
					}
				}
			}

			public void endElement(String uri, String localName, String qName) throws SAXException {

				if(qName.equals("node")) {
					double[] info = new double[2]; 
					info[0] = interseccion.getLat(); 
					info[1] = interseccion.getLon(); 
					graphData.addVertex(interseccion.getId(), info);
					dataInter.put(interseccion.getId(),interseccion);
				}
				if(qName.equals("way")) {
					if(contieneTag == true) { // Revisa que el <way> actual contenga el valor <Tag k= "highway">
						dataWay.enqueue(way);
						contieneTag = false; 
						wayTag = false; 

						Iterator<Long> it = way.getNd().iterator(); //Iterador para generar los arcos
						boolean guardadoAnterior = false; 
						long anterior = 0; 
						
		
						while(it.hasNext()){
							long firstKey = it.next(); 
							double lat1 = dataInter.get(firstKey).getLat(); 
							double lon1 = dataInter.get(firstKey).getLon();
							
							
							if(!guardadoAnterior) {
								long secondKey = it.next(); 
								anterior = secondKey; 	
								double lat2 = dataInter.get(secondKey).getLat(); 
								double lon2 = dataInter.get(secondKey).getLon(); 
								double info = Haversine.distance(lat1, lon1, lat2, lon2); 
								graphData.addEdge(firstKey, secondKey, info);	
								guardadoAnterior = true;
							}
							else if(guardadoAnterior) {
								double lat2Ant = dataInter.get(anterior).getLat(); 
								double lon2Ant = dataInter.get(anterior).getLon(); 
								double info = Haversine.distance(lat1, lon1, lat2Ant, lon2Ant); 

								graphData.addEdge(anterior, firstKey, info);
								anterior = firstKey;
								
							}
						}
					}
					else {
						wayTag = false;
					}
				}
			}

		};
		saxParser.parse(file, handler);
		hacerElJSON();
	}

	public int darTotalVertices(){
		return graphData.V();

	}

	public int darTotalArcos(){
		return graphData.E();
	}
	
	public static RedBlackBST<Long, Interseccion> getDataInter(){
		return dataInter;
	}
	
	public static Graph<Long, double[], Double> getGraph() {
		return graphData;
	}
	public void hacerElJSON ()
	{
		//Hago el JSON
		//Gson gson = new GsonBuilder().setPrettyPrinting().create();
		Gson gson = new Gson();
		String representacionBonita = gson.toJson(graphData);
		try {
			File f = new File(JSON);
			f.delete();
			f.createNewFile();

		} catch (IOException ioe) {

		}
		try {
			Files.write(Paths.get(JSON), representacionBonita.getBytes(), StandardOpenOption.APPEND);
		}catch (IOException e) {
			//exception handling left as an exercise for the reader
		}



	}
	public void cargarJson()
	{
		Gson gson = new Gson();
		graphData = null;
		try (BufferedReader br = new BufferedReader(new FileReader(JSON))) {
			String string = br.readLine();

			graphData= gson.fromJson(string, Graph.class);

		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}
	
	public void paintMap() {
		
		JFrame frame = new JFrame("Washigton DC");
		
		setOnMapReadyHandler(new MapReadyHandler() {
			
			@Override
			public void onMapReady(MapStatus status) {
				// TODO Auto-generated method stub
				if(status == MapStatus.MAP_STATUS_OK) {
					
					final Map map = getMap();
					MapOptions mapOptions = new MapOptions(); 
					MapTypeControlOptions controlOptions = new MapTypeControlOptions(); 
					controlOptions.setPosition(ControlPosition.RIGHT_TOP);
					mapOptions.setMapTypeControlOptions(controlOptions);
					map.setOptions(mapOptions);
					map.setCenter(new LatLng(38.8963324, -77.0380200));
					map.setZoom(3.0);
					
					Iterable<Long> iterableKeys = dataInter.keys();
					Iterator<Long> itKeys = iterableKeys.iterator();
					
					while(itKeys.hasNext()) {
						long key = itKeys.next();
						Iterator<Long> itVertex = graphData.adj(key); 
						LatLng[] path = new LatLng[graphData.getMyAdjList().get(key).size()];
						int totalLatLng = 0; 
						boolean agregado = false;
						
						
						while(itVertex.hasNext()) {
							Long first =  itVertex.next();
							Interseccion vertice = dataInter.get(first);
							
							for(int i = totalLatLng; i < path.length && agregado; i++) {
								path[i] = new LatLng(vertice.getLat(), vertice.getLon()); 
								agregado = true; 
							}
							agregado = false;
						}
						
						Polyline polyline = new Polyline(map); 
						polyline.setPath(path);
						
						PolylineOptions options = new PolylineOptions(); 
						options.setGeodesic(true);
						
						options.setStrokeColor("#FF0000");
						options.setStrokeOpacity(1.0);
						options.setStrokeWeight(2.0);
						
						polyline.setOptions(options);
					}
					
				}
				
			}
		});
		
		frame.add(this, BorderLayout.CENTER);
		frame.setSize(700, 500);
		frame.setVisible(true);
		
		
	}

}
