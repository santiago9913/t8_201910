package model.estructuras;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Queue<E> implements IQueue<E>
{
	public NodoListaSencilla<E> getPrimerNodo() {
		return primerNodo;
	}

	public void setPrimerNodo(NodoListaSencilla<E> primerNodo) {
		this.primerNodo = primerNodo;
	}

	public NodoListaSencilla<E> getUltimoNodo() {
		return ultimoNodo;
	}

	public void setUltimoNodo(NodoListaSencilla<E> ultimoNodo) {
		this.ultimoNodo = ultimoNodo;
	}

	public int getCantidadElementos() {
		return cantidadElementos;
	}

	public void setCantidadElementos(int cantidadElementos) {
		this.cantidadElementos = cantidadElementos;
	}

	/**
	 * Atributo que representa el primer nodo
	 */
	private NodoListaSencilla<E> primerNodo; 
	
	/**
	 * Atributo que representa el ultimo nodo
	 */
	private NodoListaSencilla<E> ultimoNodo; 
	
	/**
	 * Atributo que representa la cantidad de elementos
	 */
	private int cantidadElementos; 
	
	/**
	 * Constructor de Queue
	 * @param nPrimmer elemento que se agrega al crear el nodo
	 */
	public Queue(E nPrimmer)
	{
			primerNodo = ultimoNodo = new NodoListaSencilla<E>(nPrimmer); 
			
			cantidadElementos = 1;
	}
	
	/**
	 * Constructor de Queue (Sin elemento por parametro)
	 */
	public  Queue()
	{
		primerNodo = ultimoNodo = null; 		
		cantidadElementos = 0; 
	}
	
	/**
	 * Iterador de los nodos
	 */
	@Override
	public Iterator<E> iterator() 
	{
		return new IteradorSencillo<>(primerNodo); 
		
	}

	/**
	 * Verifica si Queue no esta vacio
	 */
	@Override
	public boolean isEmpty() 
	{
		return(primerNodo == null);
	}

	/**
	 * Devuelve el tama�o actual de Queue
	 */
	@Override
	public int size()
	{
		return cantidadElementos;
	}

	/**
	 * Encola nuevos elementos a Queue 
	 */
	@Override
	public void enqueue(E e) 
	{
		NodoListaSencilla<E> viejoUltimo = ultimoNodo;
		ultimoNodo = new NodoListaSencilla<E>(e);
		ultimoNodo.cambiarElemento(e);
		ultimoNodo.cambiarSiguiente(null);
		cantidadElementos++;
		
		if(isEmpty())
		{
			primerNodo = ultimoNodo; 
		}
		else
		{
			viejoUltimo.cambiarSiguiente(ultimoNodo);
		}
		
		
	}

	/**
	 * Desencola elementos de Queue
	 */
	@Override
	public E dequeue()
	{
		if(isEmpty())
		{
			throw new NoSuchElementException("Queue vacia"); 
		}
		
		E dato = primerNodo.darElemento(); 
		primerNodo = primerNodo.darSiguiente(); 
		cantidadElementos--; 
		
		if(isEmpty())
		{
			ultimoNodo = null; 
		}
		
		return dato; 
	}

}
