package model.estructuras;

public interface IHeap<T> extends Iterable<T>
{
	/**
	 * Retorna si la cola est� vac�a o no
	 * @return true si la cola esta vacia. False si no lo esta
	 */
	public boolean isEmpty(); 
	
	/**
	 * Obtener el elemento m�ximo (sin sacarlo de la Cola); null en caso de colavac�a
	 * @return obtiene elemento mayor de la cola
	 */
	public T max(); 
	
	/**
	 * Expande la cola
	 * @param capacity nuevo tama�ao de la cola
	 */
	public void resize(int capacity);
	
	/**
	 * Agrega un elemento a la cola. Si el elemento ya existe y tiene una prioridad
	 * diferente, el elemento debe actualizarse en la cola de prioridad.
	 * @param element elemento que se quiere agregar
	 */
	public void insert(T element);
	
	/**
	 * Saca/atiende el elemento m�ximo en la cola y lo retorna; null en caso decola vac�a
	 * <pre> el elemento existe<pre>
	 * <post> el elemento es sacado, se reorganiza la cola <post>
	 * @return el elemento mayor de la lista
	 */
	public T delMax(); 
	
	/**
	 * Retorna n�mero de elementos presentes en la cola de prioridad
	 * @return tama�o de la cola
	 */
	public int size(); 
}
