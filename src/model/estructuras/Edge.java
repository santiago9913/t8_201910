package model.estructuras;

import model.estructuras.Graph.Vertex;

public class Edge<Key, Value>{
	private final Vertex v;
    private final Vertex w;
    private final Value info; 

    /**
     * Initializes an edge between vertices {@code v} and {@code w} of
     * the given {@code weight}.
     *
     * @param  v one vertex
     * @param  w the other vertex
     * @param  weight the weight of this edge
     * @throws IllegalArgumentException if either {@code v} or {@code w} 
     *         is a negative integer
     * @throws IllegalArgumentException if {@code weight} is {@code NaN}
     */
    public Edge(Vertex v, Vertex w, Value info) {
        this.v = v;
        this.w = w;
        this.info = info;
    }

    

    /**
     * Returns either endpoint of this edge.
     *
     * @return either endpoint of this edge
     */
    public Vertex either() {
        return v;
    }
    
    /**
     * Returns info of the Edge
     * @return info of the Edge
     */
    public Value info() {
    	return info;
    }

    /**
     * Returns the endpoint of this edge that is different from the given vertex.
     *
     * @param  vertex one endpoint of this edge
     * @return the other endpoint of this edge
     * @throws IllegalArgumentException if the vertex is not one of the
     *         endpoints of this edge
     */
    public Vertex other(Vertex vertex) {
        if      (vertex == v) return w;
        else if (vertex == w) return v;
        else throw new IllegalArgumentException("Illegal endpoint");
    }

    
}
