package model.estructuras;

import java.io.InvalidObjectException;
import java.util.Stack;

import model.estructuras.ListaDobementeEncadenada;
import model.estructuras.Queue;

public class LinearProbingHashST<Key extends Comparable<Key>, Value> implements IHashGenerica<Key, Value>
{
	public int getN() {
		return N;
	}

	public void setN(int n) {
		N = n;
	}

	public int getM() {
		return M;
	}

	public void setM(int m) {
		M = m;
	}

	public Key[] getKeys() {
		return keys;
	}

	public void setKeys(Key[] keys) {
		this.keys = keys;
	}

	public Value[] getValues() {
		return values;
	}

	public void setValues(Value[] values) {
		this.values = values;
	}

	public double getAlpha() {
		return alpha;
	}

	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}

	public int getNurehash() {
		return nurehash;
	}

	public void setNurehash(int nurehash) {
		this.nurehash = nurehash;
	}

	public static int getInitCapacity() {
		return INIT_CAPACITY;
	}
	/**
	 * Capacidad inicial
	 */
	private static final int INIT_CAPACITY = 97;
	
	
	/**
	 * Numero de pares (Key, Value) en la tabla
	 */
	private int N;
	
	/**
	 * Tama�o inicial del arreglo
	 */
	private int M; 
	
	/**
	 * Llaves de la tabla Hash
	 */
	private Key[] keys; 
	
	/**
	 * Valores tabla Hash 
	 */
	private Value[] values; 
	
	/**
	 * Valor de alpha para resize
	 */
	private double alpha = 0.75; 
	/**
	 * Contador de n�mero de rehashes
	 */
	private int nurehash;	 
	/**
	 * Constructor tabla de Hash
	 */
	public LinearProbingHashST(int M)
	{
		this.M = M;
		keys = (Key[]) new Comparable[M]; 
		values = (Value[]) new Object[M];
		N = 0;
		nurehash = 0;
	}
	
	public LinearProbingHashST()
	{
		this(INIT_CAPACITY);
	}
	
	
	/**
	 * Devuelve el hash de una llave contenida en la tabla
	 * @param key de la cual se quiere conocer el hash
	 * @return el valor hash del Key deseado
	 */
	private int hash(Key key)
	{
		return  (key.hashCode() & 0x7fffffff) % M;
	}
	
	/**
	 * Realiza un resize al tama�o del arreglo
	 * @param size
	 */
	private void resize(int newSize)
	{
		LinearProbingHashST<Key, Value> temp = new LinearProbingHashST<Key, Value>(newSize); 
		nurehash++;
		for(int i = 0; i < M; i++)
		{
			if(keys[i] != null)
			{
				try
				{
					temp.put(keys[i], values[i]);
				} 
				catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		keys = temp.keys;
		values = temp.values;
		M = temp.M;
	}
	
	/**
	 * 
	 * @param key
	 * @param value
	 * @throws IllegalArgumentException
	 */
	public void put(Key key, Value value) throws IllegalArgumentException
	{
		if(key == null) //Lanza excepcion en caso de key ser null. Hash no puede recibirlo 
		{
			throw new IllegalArgumentException("Value es null"); 
		}
		
		if(value == null)
		{
			delete(key);
			return; 
		}	
		
		if(N >= M/2)
		{
			resize(2*M);
		}
		
		int i;
		for(i = hash(key); keys[i] != null; i = (i+1) % M)
		{
			if(keys[i].equals(key))
			{
				values[i] = value;
				return; 
			}
		}
		
		keys[i] = key;
		values[i] = value;
		N++; 
	}
	
	public void addRepeatedValues(Key key, Value value)
	{
		
		Stack<Value> rep = new Stack<Value>();
		
		if(key == null) //Lanza excepcion en caso de key ser null. Hash no puede recibirlo 
		{
			throw new IllegalArgumentException("Value es null"); 
		}
		
		if(value == null)
		{
			delete(key);
			return; 
		}	
		
		if(N >= M/2)
		{
			resize(2*M);
		}
		
		int i;
		for(i = hash(key); keys[i] != null; i = (i+1) % M)
		{
			if(keys[i].equals(key))
			{
				rep.push((Value) value);
				values[i] = (Value) rep;
				return; 
			}
		}
		rep.push((Value) value);
		keys[i] = key;
		values[i] = (Value) rep;
		N++; 
	}
	
	/**
	 * 
	 * @param key
	 */
	public Value delete(Key key)
	{
		if(key == null)
		{
			throw new IllegalArgumentException("Key es null");
		}
		
		if(!contains(key))
			return null;
		
		int i = hash(key);
		
		while(!key.equals(keys[i]))
		{
			i = (i+1) % M; 
		}
		
		keys[i] = null; 
		values[i] = null; 
		
		i = (i+1) % M; 
		
		while(keys[i] != null)
		{
			Key keyToRehash = keys[i]; 
			Value valueToRehash = values[i]; 
			
			keys[i] = null; 
			values[i] = null; 
			N--; 
			put(keyToRehash, valueToRehash); 
			i = (i+1) % M; 
		}
		
		N--; 
		
		if(N > 0 && N <= M/8)
		{
			resize(M/2);
		}
		return null;
	}
	
	public boolean contains(Key key)
	{
		if(key == null)
		{
			throw new IllegalArgumentException("Key es null");
		}
		
		return get(key) != null;
		
	}

	/**
	 * Devuelve el elemento con la llave buscada 
	 * @param key Key del elemento que se desea buscar 
	 * @return elemento con el key buscado. Null en el caso de no existir
	 */
	public Value get(Key key) 
	{
		if(key == null)
		{
			throw new IllegalArgumentException("Key es null");
		}
		
		for(int i = hash(key); keys[i] != null; i = (i+1) % M)
		{
			if(keys[i].equals(key))
			{
				return values[i];
			}
		}
		
		return null; 
	}
	
	 /**
     * Returns all keys in this symbol table as an {@code Iterable}.
     * To iterate over all of the keys in the symbol table named {@code st},
     * use the foreach notation: {@code for (Key key : st.keys())}.
     *
     * @return all keys in this symbol table
     */
    public Iterable<Key> keys() {
        Queue<Key> queue = new Queue<Key>();
        for (int i = 0; i < M; i++)
            if (keys[i] != null) queue.enqueue(keys[i]);
        return queue;
    }
    /**
     * Returns number of rehashes 
     * @return rehashes
     */
    public int getrehashes ()
    {
    	return nurehash;
    }
    /**
	 * Returns the number of key-value pairs in this symbol table.
	 *
	 * @return the number of key-value pairs in this symbol table
	 */
	public int size() {
		return N;
	} 
	/**
	 * Returns the size of array
	 *
	 * @return the the size of array
	 */
	public int sizeArray() {
		return M;
	} 
	
}
