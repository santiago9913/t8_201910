/**
 * Adaptado de https://www2.cs.duke.edu/courses/cps100e/fall10/class/11_Bacon/code/Graph.html
 */
package model.estructuras;

import java.util.HashMap;
import java.util.Iterator;


public class Graph<Key extends Comparable<Key>, Value,A>
{

	public int getE() {
		return E;
	}

	public void setE(int e) {
		E = e;
	}

	public int getV() {
		return V;
	}

	public void setV(int v) {
		V = v;
	}

	public SeparateChainingHash<Key, Vertex> getMyVertices() {
		return myVertices;
	}

	public void setMyVertices(SeparateChainingHash<Key, Vertex> myVertices) {
		this.myVertices = myVertices;
	}

	public SeparateChainingHash<Key, Queue<Edge>> getMyAdjList() {
		return myAdjList;
	}

	public void setMyAdjList(SeparateChainingHash<Key, Queue<Edge>> myAdjList) {
		this.myAdjList = myAdjList;
	}
	public class Vertex{

		private Key id; 
		private Value info;

		/**
		 * Crea un nuevo vertice 
		 * @param id del vertice 
		 * @param info Iformacion que contiene
		 */
		public Vertex(Key id, Value info) {
			this.id = id; 
			this.info = info;
		}

		/**
		 * Retorna el id del vertice 
		 * @return el id del vertice 
		 */
		public Key getId() {
			return id; 
		}

		/**
		 * Retorna la informacion del vertice 
		 * @return la informacion del vertice
		 */
		public Value getInfo() {
			return info;
		}

		/**
		 * Asigna un nuevo Id al vertice 
		 * @param nId nuevo Id
		 */
		public void setId(Key nId) {
			this.id = nId;
		}

		/**
		 * Asigan una nueva informacion al vertice
		 * @param nInfo nueva informacion
		 */
		public void setInfo(Value nInfo) {
			this.info = nInfo;
		}
	}

	public class Edge{
		

		public Vertex getV() {
			return v;
		}

		private final Vertex v;
		private final Vertex w;
		private A info; 

		/**
		 * Initializes an edge between vertices {@code v} and {@code w} of
		 * the given {@code weight}.
		 *
		 * @param  v one vertex
		 * @param  w the other vertex
		 * @param  weight the weight of this edge
		 * @throws IllegalArgumentException if either {@code v} or {@code w} 
		 *         is a negative integer
		 * @throws IllegalArgumentException if {@code weight} is {@code NaN}
		 */
		public Edge(Vertex v, Vertex w, A info) {
			this.v = v;
			this.w = w;
			this.info = info;
		}



		/**
		 * Returns either endpoint of this edge.
		 *
		 * @return either endpoint of this edge
		 */
		public Vertex either() {
			return v;
		}
		

		/**
		 * Returns the endpoint of this edge that is different from the given vertex.
		 *
		 * @param  vertex one endpoint of this edge
		 * @return the other endpoint of this edge
		 * @throws IllegalArgumentException if the vertex is not one of the
		 *         endpoints of this edge
		 */
		public Vertex other(Vertex vertex) {
			if      (vertex == v) return w;
			else if (vertex == w) return v;
			else throw new IllegalArgumentException("Illegal endpoint");
		}

		/**
		 * Returns info of the Edge
		 * @return info of the Edge
		 */
		public A getInfo() {
			return info;
		}

		public void setInfo(A infoArc) {
			this.info = infoArc;	
		}
		
		public Vertex getW() {
			return w;
		}
	}
	private int E; 

	private int V; 

	private SeparateChainingHash<Key, Vertex> myVertices;

	private SeparateChainingHash<Key,Queue<Edge>> myAdjList; 

	/**
	 * Crea un nuevo Grafo sin vertices y sin arcos
	 */
	public Graph()
	{
		this.E = 0; 
		this.V = 0; 
		this.myVertices = new SeparateChainingHash();
		this.myAdjList = new SeparateChainingHash();
	}

	/**
	 * Numero de vertices 
	 * @return vertices en el Grafo 
	 */
	public int V()
	{
		return V;
	}

	/**
	 * Numero de Arcos
	 * @return arcos en el Grafo
	 */
	public int E()
	{
		return E; 
	}

	/**
	 * Retorna True si el Vertice con el id esta en el grafo. False si no lo esta
	 * @param id del vertice buscado
	 * @return True si el vertice esta. False si no esta 
	 */
	public boolean hasVertex(Key id) {
		return myVertices.contains(id);
	}

	/**
	 * Adiciona un vertice con un Id unico
	 * @param idVertex ID del vertice
	 * @param infoVertex Informacion que contiene el vertice
	 */
	public void addVertex(Key idVertex, Value infoVertex) {
		Vertex v;
		v = myVertices.get(idVertex); 
		if (idVertex == null)
			System.out.println("BUEnAS");
		if(v == null) {
			v = new Vertex(idVertex, infoVertex); 
			myVertices.put(idVertex, v);
			myAdjList.put(idVertex, new Queue<Edge>());
			V++;
		}

	}

	/**
	 * Retorna True si existe un arco desde los dos Id
	 * @param from Id desde donde se quiere buscar el arco 
	 * @param to Destino a donde se quiere encontrar el arco
	 * @return True si exsite el arco. False sino existe
	 */
	public boolean hasEdge(Key from, Key to) {
		if(!hasVertex(from) || !hasVertex(to)) {
			return false;
		}	
		Queue <Edge> a= myAdjList.get(from);
		if (a == null)
			return false;
		Iterator <Edge > it= a.iterator();
		
		while (it.hasNext())
		{
			Edge b= it.next();
			if (b.getW().getId().equals(to) || b.either().getId().equals(to))
				return true;
		}
		return false;





	}

	/**
	 * Adiciona el arco No dirigido entre el vertice IdVertexIni y el vertice IdVertexFin. El Arco tiene la informacion infoArc
	 * @param idVertexIni
	 * @param idVertexFin
	 * @param infoArc
	 */
	public void addEdge(Key idVertexIni, Key idVertexFin, A infoArc)
	{
		Vertex v, w;

		if (hasEdge(idVertexIni, idVertexFin)) {
			return;
		}
		E++;
		v = getVertex(idVertexIni);
		if(v == null) {
			v = new Vertex(idVertexFin, null); 
			addVertex(v.getId(), v.getInfo());
		}
		w = getVertex(idVertexFin);
		if(w == null) {
			w = new Vertex(idVertexFin, null);
			addVertex(w.getId(), w.getInfo());
		}
		Edge a= new Edge(v, w, infoArc);
		myAdjList.get(idVertexIni).enqueue(a);
		myAdjList.get(idVertexFin).enqueue(a);
	}

	/**
	 * Retorna un vertice con el id buscado 
	 * @param id del vertice buscado
	 * @return vertice con el id buscado 
	 */
	public Vertex getVertex(Key id) {
		return myVertices.get(id);
	}

	/**
	 * Obtener la información de un vértice
	 * @param idVertex Id del vertice que se quiere 
	 * @return información de un vértice
	 */
	public Value getInfoVertex(Key idVertex) {
		return myVertices.get(idVertex).getInfo();
	}

	/**
	 * Modificar la información del vértice idVertex
	 * @param idVertex Vertice que se quiere modificar 
	 * @param infoVertex informacion nueva que entra 
	 */
	public void setInfoVertex(Key idVertex, Value infoVertex) {
		myVertices.get(idVertex).setInfo(infoVertex);
	}

	/**
	 * Obtener la información de un arco
	 * @param idVertexIni Id vertice inicio 
	 * @param idVertexFin Id vertice fin
	 * @return información de un arco
	 */
	public A getInfoArc(Key idVertexIni, Key idVertexFin) {
		Queue<Edge> connections = myAdjList.get(idVertexIni);
		Iterator<Edge> it = connections.iterator();
		A info = null;

		boolean found = false; 

		while(found == false) {
			Edge now = it.next(); 
			Vertex start = now.either();
			Vertex end = now.getW();
			if((start.getId().equals(idVertexIni) && end.getId().equals(idVertexFin))|| (end.getId().equals(idVertexIni) && start.getId().equals(idVertexFin))) {
				found = true;
				info = now.getInfo();
			}
			else {
				continue;
			}
		}
		return info;
	}

	/**
	 * Modificar la información del arco entre los vértices idVertexIni e idVertexFin
	 * @param idVertexIni Id vertice inicio 
	 * @param idVertexFin Id vertice fin
	 * @param infoArc info nueva 
	 */
	public void setInfoArc(Key idVertexIni, Key idVertexFin,A infoArc) {
		Queue<Edge> connections = myAdjList.get(idVertexIni);
		Iterator<Edge> it = connections.iterator();

		boolean found = false; 
		
		while(found == false) {
			Edge now = it.next(); 
			Vertex start = now.either();
			Vertex end = now.getW();

			if((start.getId().equals(idVertexIni) && end.getId().equals(idVertexFin))|| (end.getId().equals(idVertexIni) && start.getId().equals(idVertexFin))) {
				found = true;
				now.setInfo(infoArc);
				break;
			}
			else {
				continue;
			}
		}
	}
	/**
	 * Retorna los identificadores de los vértices adyacentes a idVertex
	 * @param idVertex vertice el cual se quieren saber los adyacentes
	 * @return un iterator con los identificadores de los vértices adyacentes a idVertex
	 */
	public Iterator<Key> adj(Key idVertex)
	{
		Queue <Key> res= new Queue <Key>();
		if(!hasVertex( idVertex) ) {
			return res.iterator();
		}	
		Queue <Edge> a= myAdjList.get( idVertex);
		if (a == null)
			return res.iterator();
		Iterator <Edge > it= a.iterator();
		
		while (it.hasNext())
		{
			Edge b= it.next();
			res.enqueue(b.other(myVertices.get(idVertex)).getId());
			
		}
		return res.iterator();
	}

}